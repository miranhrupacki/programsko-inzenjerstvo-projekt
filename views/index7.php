<?php
include('../components/header.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>Nijanse Pro</title>

  <meta charset="utf-8" />
  <meta name="description" content="web-project" />
  <meta name="author" content="web-project" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="../css/index3.css" />
  <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet" />
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
</head>

<body>
  <main>

    <img name="slide" width="80%" height="400" class="img-slider fade" />
    <?php include('../components/footer.php'); ?>

  </main>
  <script>
    let i = 0; // Start Point
    let images = []; // Images Array
    let time = 2000; // Time Between Switch

    // Image List
    images[0] = "../assets/aa.jpg";
    images[1] = "../assets/tour_img-1096032-146.jpg";
    images[2] = "../assets/vistas-manhattan-noche.jpg";

    // Change Image
    function changeImg() {
      document.slide.src = images[i];

      // Check If Index Is Under Max
      if (i < images.length - 1) {
        // Add 1 to Index
        i++;
      } else {
        // Reset Back To O
        i = 0;
      }

      // Run function every x seconds
      setTimeout("changeImg()", time);
    }

    // Run function when page loads
    window.onload = changeImg;
  </script>
</body>

</html>