<?php include('../components/header.php'); ?>

<!DOCTYPE html>

<html lang="en">

<head>
  <title>O nama</title>

  <meta charset="utf-8" />
  <meta name="description" content="web-project" />
  <meta name="author" content="web-project" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="../css/index3.css" />
  <link rel="stylesheet" href="../css/aboutUs.css" />
  <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet" />
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
</head>

<body>
  <main>

    <section class="about-us-section">
      <h3 class="introduction">
        Nijanse Pro je malo poduzeće koje je nastalo 2016. godine te se bavimo građevinskim radovima, pružamo usluge za izradu, obnovu ili uređenje Vašeg doma. Trenutno brojimo 15 vrijednih radnika. Tvrtka raspolaže sa dva kombija koji imaju sanduk i ceradu, dva caddy-a i jednim višenamjenskim bagerom (kašika za utovar, kašika za iskop). Vrste posla koje obavljamo su: uređenje kuće (vanjsko i unutarnje), betoniranje prilaza, obnova krova, izrada vikend kuća, sanitarija i izrada komarnika.
      </h3>
    </section>

    <section class="about-us-slider">
      <img name="slide" width="80%" height="400" class="img-slider fade" />
    </section>

    <?php include('../components/footer.php'); ?>

  </main>
  <script>
    let i = 0; // Start Point
    let images = []; // Images Array
    let time = 2000; // Time Between Switch

    // Image List
    images[0] = "../assets/dnevna.jpg";
    images[1] = "../assets/sanitarija.jpg";
    images[2] = "../assets/kuca.jpg";
    images[3] = "../assets/pecenjara.jpg";
    images[4] = "../assets/terasa.jpg";
    images[5] = "../assets/ograda.jpg";
    images[6] = "../assets/vikendica.jpg";
    images[7] = "../assets/bager.jpg";

    // Change Image
    function changeImg() {
      document.slide.src = images[i];

      // Check If Index Is Under Max
      if (i < images.length - 1) {
        // Add 1 to Index
        i++;
      } else {
        // Reset Back To O
        i = 0;
      }

      // Run function every x seconds
      setTimeout("changeImg()", time);
    }

    // Run function when page loads
    window.onload = changeImg;
  </script>
  <script type="text/javascript" src="../js/mobile.js"></script>
</body>

</html>