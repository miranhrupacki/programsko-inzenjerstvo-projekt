<?php include('../components/header.php'); ?>

<!DOCTYPE html>

<html lang="en">

<head>
  <title>Pronađi nas</title>

  <meta charset="utf-8" />
  <meta name="description" content="web-project" />
  <meta name="author" content="web-project" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="../css/index3.css" />
  <link rel="stylesheet" href="../css/map1.css" />
  <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet" />
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
</head>

<body>
  <main>

    <head>
      <title>Simple Map</title>
      <meta name="viewport" content="initial-scale=1.0">
      <meta charset="utf-8">
    </head>

    <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22299.449834693773!2d18.141523482694016!3d45.68231734504338!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475d3fe6305eb513%3A0xbe18af87b279fe9d!2sGolinci!5e0!3m2!1shr!2shr!4v1579638600688!5m2!1shr!2shr" width="600" height="450" frameborder="0" style="border:0;"></iframe></div>

    <?php include('../components/footer.php'); ?>

    <script>
      var map;

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {
            lat: -34.397,
            lng: 150.644
          },
          zoom: 2
        });
      }
    </script>
</body>

</html>