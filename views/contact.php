<?php include('../components/header.php'); ?>

<!DOCTYPE html>

<html lang="en">

<head>
    <title>Kontakt</title>

    <meta charset="utf-8" />
    <meta name="description" content="web-project" />
    <meta name="author" content="web-project" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../css/index3.css" />
    <link rel="stylesheet" href="../css/contact1.css" />
    <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
</head>

<body>
    <main class="wrapper">
        <h2>Ukoliko ste zainteresirani za suradnju sa tvrtkom Nijanse Pro slobodno nas kontaktirajte na naš broj, email ili na našoj adresi.</h2>

        <ul class="list">
            <li class="list-items">Mobitel: 095/5555-000</li>
            <li class="list-items">E-mail: nijansepro@gmail.com</li>
            <li class="list-items">Adresa: Kralja Tomislava 5, Golinci</li>
        </ul>

        <?php include('../components/footer.php'); ?>
    </main>
</body>

</html>