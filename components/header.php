<header style="display:flex;">
    <h1 class="logo"><a href="../views/index7.php" style="text-decoration: none; color: white;">NIJANSE PRO</a></h1>
    <nav>
        <ul class="nav__links">
            <li><a href="../views/index7.php">Naslovnica</a></li>
            <li><a href="../views/aboutUs.php">O nama</a></li>
            <li><a href="../views/contact.php">Kontakt</a></li>
            <li><a href="../views/comments.php">Ostavi komentar</a></li>
            <li><a class="cta" href="../views/logout1.php">Log Out</a></li>
        </ul>
    </nav>

    <p onclick="openNav()" class="menu cta">Menu</p>
</header>

<div id="mobile__menu" class="overlay">
    <a class="close" onclick="closeNav()">&times;</a>
    <div class="overlay__content">
        <a href="../views/index7.php">Naslovnica</a>
        <a href="../views/aboutUs.php">O nama</a>
        <a href="../views/contact.php">Kontakt</a>
        <a href="../views/comments.php">Ostavi komentar</a>
        <a class="cta" href="../views/logout1.php">Log Out</a>
    </div>
</div>

<script type="text/javascript" src="../js/mobile.js"></script>