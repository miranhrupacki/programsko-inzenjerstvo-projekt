<footer class="footer" id="footer">
    <div>
        <h3>Nijanse Pro</h3>

        <ul class="list">
            <li class="footer-p">Kralja Tomislava 5, Golinci</li>
            <li class="footer-p">Hrvatska</li>
        </ul>
    </div>

    <div>
        <h3>Kontakt</h3>

        <ul class="list">
            <li class="footer-p">095/5555-000</li>
            <li class="footer-p">nijansepro@gmail.com</li>
        </ul>
    </div>

    <div>
        <h3>Društvene mreže</h3>

        <div class="social-icons">
            <a href="https://hr-hr.facebook.com/nijanse.pro/" target="_blank" ><i class="fa fa-facebook-square"></i></a>
            <i class="fa fa-linkedin"></i>
            <i class="fa fa-youtube"></i>
        </div>
    </div>

    <div>
        <h3>Pronađi nas!</h3>
        <a href="../views/map.php"><img src="../assets/icons8-google-maps-40.png" alt="globe" class="icon-globe" /></i>
    </div>
</footer>